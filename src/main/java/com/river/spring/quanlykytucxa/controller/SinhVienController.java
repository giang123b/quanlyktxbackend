package com.river.spring.quanlykytucxa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.river.spring.quanlykytucxa.model.SinhVien;
import com.river.spring.quanlykytucxa.repository.SinhVienRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class SinhVienController {

	private static final Logger logger = LoggerFactory.getLogger(SinhVienController.class);

	@Autowired
	SinhVienRepository sinhVienRepository;

	@GetMapping("/sinhviens")
	public ResponseEntity<List<SinhVien>> getAllSinhViens(@RequestParam(required = false) String hoTen) {
		try {
			List<SinhVien> sinhViens = new ArrayList<SinhVien>();

			if (hoTen == null)
				sinhVienRepository.findAll().forEach(sinhViens::add);
			else
				sinhVienRepository.findByHoTenContaining(hoTen).forEach(sinhViens::add);

			if (sinhViens.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(sinhViens, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/sinhviens/{maSinhVien}")
	public ResponseEntity<SinhVien> getSinhVienById(@PathVariable("maSinhVien") String maSinhVien) {
		Optional<SinhVien> sinhVienData = sinhVienRepository.findByMaSinhVienContaining(maSinhVien);

		if (sinhVienData.isPresent()) {
			return new ResponseEntity<>(sinhVienData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@PostMapping("/sinhviens")
	public ResponseEntity<SinhVien> createSinhVien(@RequestBody SinhVien sinhVien) {
		try {
			SinhVien _sinhVien = sinhVienRepository.save(sinhVien);
			return new ResponseEntity<>(_sinhVien, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/sinhviens/{maSinhVien}")
	public ResponseEntity<SinhVien> updateSinhVien(@PathVariable("maSinhVien") String maSinhVien, @RequestBody SinhVien sinhVien) {
		Optional<SinhVien> sinhVienData = sinhVienRepository.findByMaSinhVienContaining(maSinhVien);

		if (sinhVienData.isPresent()) {
			SinhVien _sinhVien = sinhVienData.get();
			_sinhVien.setMaSinhVien(sinhVien.getMaSinhVien());
			_sinhVien.setHoTen(sinhVien.getHoTen());
			_sinhVien.setSoCMT(sinhVien.getSoCMT());
			_sinhVien.setNgaySinh(sinhVien.getNgaySinh());
			_sinhVien.setLop(sinhVien.getLop());
			_sinhVien.setQueQuan(sinhVien.getQueQuan());
			return new ResponseEntity<>(sinhVienRepository.save(_sinhVien), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * nếu bạn để ResponseEntity<?> thì bạn trả về kiểu gì cũng được, ví dụ như này này, khi xóa thành công sẽ trả về code 200 kèm theo message: xóa thành công
	 *
	 * @param maSinhVien
	 * @return
	 */
	@DeleteMapping("/sinhviens/{maSinhVien}")
	public ResponseEntity<?> deleteSinhVien(@PathVariable("maSinhVien") String maSinhVien) {
		logger.info("xoa sinh vien voi ma sinh vien: {}", maSinhVien);
		SinhVien sinhVien = sinhVienRepository.findByMaSinhVien(maSinhVien);
		if (sinhVien != null){
			sinhVienRepository.deleteById(maSinhVien);
			return new ResponseEntity<>("Xoa Thanh Cong", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

//	@DeleteMapping("/sinhviens")
//	public ResponseEntity<HttpStatus> deleteAllSinhViens() {
//		try {
//			sinhVienRepository.deleteAll();
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}

//	}

}
