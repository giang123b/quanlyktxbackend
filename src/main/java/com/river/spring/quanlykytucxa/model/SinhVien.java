package com.river.spring.quanlykytucxa.model;

import javax.persistence.*;

@Entity
@Table(name = "sinh_vien")
public class SinhVien {
	
	@Id
	@Column(name = "ma_sinh_vien")
	private String maSinhVien;

	@Column(name = "ho_ten")
	private String hoTen;
	
	@Column(name = "soCMT")
	private String soCMT;

	@Column(name = "ngay_sinh")
	private String ngaySinh;

	@Column(name = "lop")
	private String lop;

	@Column(name = "que_quan")
	private String queQuan;

	public SinhVien() {

	}

	// em cung k biet luon
	public SinhVien(String maSinhVien, String hoTen, String soCMT, String ngaySinh, String lop, String queQuan) {
		super();
		this.maSinhVien = maSinhVien;
		this.hoTen = hoTen;
		this.soCMT = soCMT;
		this.ngaySinh = ngaySinh;
		this.lop = lop;
		this.queQuan = queQuan;
	}


	public String getMaSinhVien() {
		return maSinhVien;
	}


	public void setMaSinhVien(String maSinhVien) {
		this.maSinhVien = maSinhVien;
	}


	public String getHoTen() {
		return hoTen;
	}


	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}


	public String getSoCMT() {
		return soCMT;
	}


	public void setSoCMT(String soCMT) {
		this.soCMT = soCMT;
	}


	public String getNgaySinh() {
		return ngaySinh;
	}


	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}


	public String getLop() {
		return lop;
	}


	public void setLop(String lop) {
		this.lop = lop;
	}


	public String getQueQuan() {
		return queQuan;
	}


	public void setQueQuan(String queQuan) {
		this.queQuan = queQuan;
	}


	@Override
	public String toString() {
		return "[maSinhVien=" + maSinhVien + ", hoTen=" + hoTen + 
				", soCMT=" + soCMT + ", lop=" + lop + 
				", queQuan=" + queQuan +"]";
	}

}
