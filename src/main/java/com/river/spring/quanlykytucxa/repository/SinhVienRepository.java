package com.river.spring.quanlykytucxa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.river.spring.quanlykytucxa.model.SinhVien;
import org.springframework.stereotype.Repository;

@Repository
public interface SinhVienRepository extends JpaRepository<SinhVien, String> {
	List<SinhVien> findByHoTenContaining(String hoTen);
	Optional<SinhVien> findByMaSinhVienContaining(String maSinhVien);
	SinhVien findByMaSinhVien(String ma);

}
